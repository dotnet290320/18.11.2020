﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL1811
{
    class Visit : IPoco
    {
        public int Id { get; set; }
        public string FirstName{ get; set; }
        public string LastName { get; set; }
        public DateTime VisitedAt { get; set; }
        public string Phone { get; set; }
        public int StoreId { get; set; }

        public Visit()
        {

        }

        // <Person> <Id>1</Id><Name>John</Name> </Person> --- XML
        // { ClassName : 'Person', Id : 1, Name : 'John  } -- JSON
        // [ { Id : 1, name : 'John' } , { Id : 2, name : 'Suzi' } ]
        // { Name : 'Dan' , Address : { City : 'Tel Aviv', Stree : 'King George' } }

        public override string ToString()
        {
            return $"{Newtonsoft.Json.JsonConvert.SerializeObject(this).Replace(",", "\n")}";
            //return base.ToString();
        }

    }
}
