using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL1811
{using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL1811
{
    class VisitsDAO
    {
        private string m_conn_string;

        public VisitsDAO(string conn_string)
        {
            m_conn_string = conn_string;
        }

        private int ExecuteNonQuery(string query)
        {
            int result = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                using (cmd.Connection = new SqlConnection(m_conn_string))
                {
                    cmd.Connection.Open();

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;

                    result = cmd.ExecuteNonQuery();
                }
            }
            
            return result;
        }

        public void DropTable()
        {
            ExecuteNonQuery("DROP TABLE VISITS");
        }

        public void CreateTable()
        {
            ExecuteNonQuery("CREATE TABLE VISITS (" +
                                        "visit_id INT PRIMARY KEY IDENTITY(1, 1)," +
                                        "first_name VARCHAR(50) NOT NULL," +
                                        "last_name VARCHAR(50) NOT NULL," +
                                        "visited_at DATETIME," +
                                        "phone VARCHAR(20)," +
                                        "store_id INT NOT NULL);");
        }

        public void AddVisit(Visit v)
        {
            ExecuteNonQuery("INSERT INTO VISITS(first_name, last_name, visited_at, phone, store_id)" +
            $"VALUES('{v.FirstName}', '{v.LastName}', '{v.VisitedAt}', '{v.Phone}', {v.StoreId});");

        }

        public List<Visit> GetAllVisits()
        {
            List<Visit> result = new List<Visit>();

            using (SqlCommand cmd = new SqlCommand())
            {
                using (cmd.Connection = new SqlConnection(m_conn_string))
                {
                    cmd.Connection.Open();

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT * FROM VISITS";

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Visit v = new Visit
                        {
                            Id = (int)reader["visit_id"],
                            FirstName = reader["first_name"].ToString(),
                            LastName = reader["last_name"].ToString(),
                            VisitedAt = Convert.ToDateTime(reader["visited_at"]),
                            Phone = reader["phone"].ToString(),
                            StoreId = (int)reader["store_id"]
                        };
                        result.Add(v);
                    }

                }
            }

            return result;
        }
        public Visit GetVisitById(int id)
        {
            Visit result = null;

            using (SqlCommand cmd = new SqlCommand())
            {
                using (cmd.Connection = new SqlConnection(m_conn_string))
                {
                    cmd.Connection.Open();

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = $"SELECT * FROM VISITS WHERE visit_id={id}";

                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        result = new Visit
                        {
                            Id = (int)reader["visit_id"],
                            FirstName = reader["first_name"].ToString(),
                            LastName = reader["last_name"].ToString(),
                            VisitedAt = Convert.ToDateTime(reader["visited_at"]),
                            Phone = reader["phone"].ToString(),
                            StoreId = (int)reader["store_id"]
                        };
                    }

                }
            }

            return result;
        }

        public void UpdateVisit(Visit v, int id)
        {
            int result = ExecuteNonQuery(
                $"UPDATE FROM VISITS SET first_name={v.FirstName}, last_name={v.LastName}, visited_at={v.VisitedAt},"+
                $"phone={v.Phone}, store_id={v.StoreId} WHERE visit_id={id}");
        }

        public int RemoveVisit(int id)
        {
            int result = ExecuteNonQuery($"DELETE * FROM VISITS WHERE visit_id={id}");
            return result;
        }

        public List<Visit> GetVisitsForSpecificDay(DateTime dateTime)
        {
            // Etgar
            return null;
        }

    }
}

    class VisitsDAO
    {
        private string m_conn_string;

        public VisitsDAO(string conn_string)
        {
            m_conn_string = conn_string;
        }

        private int ExecuteNonQuery(string query)
        {
            int result = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                using (cmd.Connection = new SqlConnection(m_conn_string))
                {
                    cmd.Connection.Open();

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;

                    result = cmd.ExecuteNonQuery();
                }
            }
            
            return result;
        }

        public void DropTable()
        {
            ExecuteNonQuery("DROP TABLE VISITS");
        }

        public void CreateTable()
        {
            ExecuteNonQuery("CREATE TABLE VISITS (" +
                                        "visit_id INT PRIMARY KEY IDENTITY(1, 1)," +
                                        "first_name VARCHAR(50) NOT NULL," +
                                        "last_name VARCHAR(50) NOT NULL," +
                                        "visited_at DATETIME," +
                                        "phone VARCHAR(20)," +
                                        "store_id INT NOT NULL);");
        }

        public void AddVisit(Visit v)
        {
            // fix this!!!!
            //ExecuteNonQuery("INSERT INTO VISITS(first_name, last_name, visited_at, phone, store_id)" +
            //"VALUES('MAOR', 'COHEN', '2020-11-18', '03555555', 1);");

        }

        public List<Visit> GetAllVisits()
        {
             //using (SqlCommand cmd = new SqlCommand())
            //{
            //    using (cmd.Connection = new SqlConnection(conn_string))
            //    {
            //        cmd.Connection.Open();

            //        cmd.CommandType = System.Data.CommandType.Text;
            //        cmd.CommandText = "SELECT * FROM ORDERS";

            //        SqlDataReader reader = cmd.ExecuteReader();

            //        while (reader.Read())
            //        {
            //            Console.WriteLine($"{reader["ID"]}");
            //        }

            //    }
            //}

            return null;
        }
        public Visit GetVisitById(int id)
        {
            return null;
        }

        public void UpdateVisit(Visit v, int id)
        {

        }

        public void RemoveVisit(int id)
        {

        }

        public List<Visit> GetVisitsForSpecificDay(DateTime dateTime)
        {
            return null;
        }

    }
}
